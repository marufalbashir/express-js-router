var express = require('express');
var app = express();

app.get('/', function(req, res){
    res.send('Hello Node JS');
});
app.post('/hello', function(req, res){
    res.send('You Just Called POST method');
    res.statusCode(200);
})

// import the router file 
var things = require('./things.js');
app.use('/things', things);

// app dynamic router 


app.listen(3000);
