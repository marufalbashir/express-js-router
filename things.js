var express = require('express');
var router = express.Router();

router.get('/hello', function(req, res){
    res.send('Get router from things.js')
})
// app dynamic router 
router.get('/:id', function(req, res){
    var reqId = req.params.id;
   
    res.send('the dynamic id is ' + req.params.id);
})
// dynamic name and id 
// router.get('/:name/:id', function(req, res){
//     res.send('id : ' + req.params.id + ' & the name is ' + req.params.name);
// });

// pattern match route
router.get('/pattern-matched/:id([0-9]{5})', function(req, res){
    res.send('id: ' + req.params.id);
})

// the 404 not found route
router.get('*', function(req, res){
    res.send('error 404 not found')
})

module.exports = router;
